<?php
    /**
     *
     */
    class candidates
    {

        function __construct() {}
        public static function connectMe()
        {
            $connect = mysqli_connect('localhost', DB_USER, DB_PASSWORD, DB_TABLE);
            if(!$connect) { echo 'Cant connet to DB'; die; } else {
				return $connect;
			}
        }

        public function insertCandidate($name, $position, $color)
        {
            $conns = self::connectMe();
            mysqli_query($conns, "insert into people (pname, ppos, pcolor) values ('".$name."', '".$position."', '".$color."')");
            header("Location: ".HOST."/?page=candidate&type=all");
        }

        public function getAllCandidates()
        {
            $dpositions = array();
            $conns = self::connectMe();
            $getPositions = mysqli_query($conns, "select * from people");
            $counter = 0;
            while ($row = mysqli_fetch_assoc($getPositions)) {
                $dpositions[$counter]["pid"] = $row["pid"];
                $dpositions[$counter]["pname"] = $row["pname"];
                $dpositions[$counter]["ppos"] = $row["ppos"];
                $dpositions[$counter]["pcolor"] = $row["pcolor"];
                $counter++;
            }
            return serialize($dpositions);
        }

        public function getCandidateByID($id)
        {
            $dcandidate = array();
            $conns = self::connectMe();
            $getCandidate = mysqli_query($conns, "select * from people where pid = ".$id);
            while ($row = mysqli_fetch_assoc($getCandidate)) {
                $dcandidate["pid"] = $row["pid"];
                $dcandidate["pname"] = $row["pname"];
                $dcandidate["ppos"] = $row["ppos"];
                $dcandidate["pcolor"] = $row["pcolor"];
            }
            return serialize($dcandidate);
        }

        public function getCandidateNameByID($id)
        {
            $name = "";
            $conns = self::connectMe();
            $getCandidate = mysqli_query($conns, "select * from people where pid = ".$id);
            while ($row = mysqli_fetch_assoc($getCandidate)) {
                $name = $row["pname"];
            }
            return $name;
        }
        public function getalldata($id)
        {
            $dcandidate = array();
            $conns = self::connectMe();
            $getCandidate = mysqli_query($conns, "select * from people where pid = ".$id);
            while ($row = mysqli_fetch_assoc($getCandidate)) {
                $dcandidate["fname"] = $row["fname"];
                $dcandidate["lname"] = $row["lname"];
                $dcandidate["nname"] = $row["nname"];
                $dcandidate["bdate"] = $row["bdate"];
                $dcandidate["address"] = $row["address"];
                $dcandidate["about"] = $row["about"];
                $dcandidate["goal"] = $row["goal"];
                $dcandidate["image"] = $row["image"];
            }
            return serialize($dcandidate);
        }

        public function updateCandidate($id, $name, $position, $color)
        {
            $conns = self::connectMe();
            mysqli_query($conns, "update people set pname = '".$name."', ppos = '".$position."', pcolor = '".$color."' where pid = ".$id);
            header("Location: ".HOST."/?page=candidate&type=all");
        }

        public function delete($id)
        {
            $conns = self::connectMe();
            mysqli_query($conns, "delete from people where pid = ".$id);
            header("Location: ".HOST."/?page=candidate&type=all");
        }

        public function uploadimage($data)
        {
            $errors= array();
            $file_name = $data['name'];
            $file_size = $data['size'];
            $file_tmp = $data['tmp_name'];
            $file_type = $data['type'];

            $get_values = explode('.',$file_name);
            $get_last_value = end($get_values);
            $file_ext = strtolower($get_last_value);

            $expensions= array("jpeg","jpg","png");
            $thelink = "";
            if(in_array($file_ext,$expensions)=== false){
                $errors[]="extension not allowed, please choose a JPEG or PNG file.";
            }

            if(empty($errors)==true){
                move_uploaded_file($file_tmp, $_SERVER['DOCUMENT_ROOT']."/uploads/".$file_name);
                $thelink = HOST."/uploads/".$file_name;
            }else{
                print_r($errors);
            }

            return $thelink;
        }

        public function updateProfile($data, $image)
        {
            // print_r($data);
            $conns = self::connectMe();
            mysqli_query($conns, "update people set fname = '".$data["fname"]."', lname = '".$data["lname"]."', nname = '".$data["nname"]."', bdate = '".$data["bmonth"]."/".$data["bdate"]."/".$data["byear"]."', address = '".$data["address"]."', about = '".$data["about"]."', goal = '".$data["goal"]."', image = '".$image."' where pid = ".$data["did"]);
            return 1;
        }
    }

?>
