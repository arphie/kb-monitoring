<?php

    /**
     *
     */
    class users
    {

        function __construct()
        {}

        public static function connectMe()
        {
            $connect = mysqli_connect('localhost', DB_USER, DB_PASSWORD, DB_TABLE);
            if(!$connect) { echo 'Cant connet to DB'; die; } else {
                return $connect;
            }
        }

        public function getAllPositions()
        {
            $dpositions = array();
            $conns = self::connectMe();
            $getPositions = mysqli_query($conns, "select * from users");
            $counter = 0;
            while ($row = mysqli_fetch_assoc($getPositions)) {
                $dpositions[$counter]["userid"] = $row["userid"];
                $dpositions[$counter]["username"] = $row["username"];
                $dpositions[$counter]["access"] = $row["access"];
                $counter++;
            }
            return serialize($dpositions);
        }

        public function savePosition($user, $pass, $access)
        {
            $conns = self::connectMe();
            mysqli_query($conns, "insert into users (username, pass, access) values ('".trim($user)."', '".md5(trim($pass))."','".trim($access)."')");
            header("Location: ".HOST."/?page=user&type=all");
        }

        public function idtonameaccess($id)
        {
            return ($id == "1" ? "Super Admin" : ($id == "2" ? "Admin" : ($id == "3" ? "Viewer" : "Undefined")));
        }

        public function getuserByID($id)
        {
            $dcandidate = array();
            $conns = self::connectMe();
            $getCandidate = mysqli_query($conns, "select * from users where userid = ".$id);
            while ($row = mysqli_fetch_assoc($getCandidate)) {
                $dcandidate["username"] = $row["username"];
                $dcandidate["access"] = $row["access"];
            }
            return serialize($dcandidate);
        }

        public function updateuser($id, $name, $access)
        {
            $conns = self::connectMe();
            mysqli_query($conns, "update users set username = '".$name."', access = '".$access."' where userid = ".$id);
            header("Location: ".HOST."/?page=user&type=all");
        }

        public function changepass($id, $pass)
        {
            $conns = self::connectMe();
            mysqli_query($conns, "update users set pass = '".$pass."' where userid = ".$id);
            header("Location: ".HOST."/?page=user&type=all");
        }

        public function delete($id)
        {
            $conns = self::connectMe();
            mysqli_query($conns, "delete from users where userid = ".$id);
            header("Location: ".HOST."/?page=user&type=all");
        }

    }


?>
