<?php

    /**
     *
     */
    class monitor
    {
        function __construct()
        {}
        public static function connectMe()
        {
            $connect = mysqli_connect('localhost', DB_USER, DB_PASSWORD, DB_TABLE);
            if(!$connect) { echo 'Cant connet to DB'; die; } else {
				return $connect;
			}
        }
        public function getCandidbyPosition($id)
        {
            $dpositions = array();
            $conns = self::connectMe();
            $getPositions = mysqli_query($conns, "select * from people where ppos = ".$id);
            $counter = 0;
            while ($row = mysqli_fetch_assoc($getPositions)) {
                $dpositions[$counter]["pid"] = $row["pid"];
                $dpositions[$counter]["pname"] = $row["pname"];
                $dpositions[$counter]["ppos"] = $row["ppos"];
                $dpositions[$counter]["pcolor"] = $row["pcolor"];
                $dpositions[$counter]["pvote"] = $row["pvote"];
                $dpositions[$counter]["image"] = $row["image"];
                $counter++;
            }
            return serialize($dpositions);
        }

        public function getPositonbyCandidate($id)
        {
            $dpositions = "";
            $conns = self::connectMe();
            $getPositions = mysqli_query($conns, "select * from people where pid = ".$id);
            while ($row = mysqli_fetch_assoc($getPositions)) { $dpositions = $row["ppos"]; }
            return $dpositions;
        }

        public function getvotes($id)
        {
            $dpositions = array();
            $conns = self::connectMe();
            $getPositions = mysqli_query($conns, "select * from monitor where candid = ".$id);
            while ($row = mysqli_fetch_assoc($getPositions)) {
                $dpositions["candid"] = $row["candid"];
                $dpositions["casts"] = $row["casts"];
            }
            return serialize($dpositions);
        }

        public function checkifvoteiscasted($id)
        {
            $dconditon = "";
            $conns = self::connectMe();
            $getPositions = mysqli_query($conns, "select count(*) as count from monitor where candid = ".$id);
            while ($row = mysqli_fetch_assoc($getPositions)) { $dconditon = $row["count"]; }
            return ($dconditon == 0 ? false : true);
        }

        public function checknumberofcandidates($id)
        {
            $dconditon = "";
            $conns = self::connectMe();
            $getPositions = mysqli_query($conns, "select count(*) as count from people where ppos = ".$id);
            while ($row = mysqli_fetch_assoc($getPositions)) { $dconditon = $row["count"]; }
            return $dconditon;
        }

        public function castVote($id, $votes)
        {
            $conns = self::connectMe();
            //check if data is already in DB
            if(self::checkifvoteiscasted($id)){
                mysqli_query($conns, "update monitor set casts = '".$votes."' where candid = '".$id."' ");
            } else {
                mysqli_query($conns, "insert into monitor (candid, casts) values ('".$id."','".$votes."')");
            }
            header("location: /".emonitor."/?page=monitor&type=vote&pos=".$id);
        }
        public function getTotalVotesPerCandidates($positionid)
        {
            // get all candidates id
            $candids = self::getCandidbyPosition($positionid);
            $dcandidates = array();
            $counter = 0;
            foreach (unserialize($candids) as $key => $value) {
                $dcandidates[$counter]["pid"] = $value["pid"];
                $dcandidates[$counter]["pname"] = $value["pname"];
                $dcandidates[$counter]["pcolor"] = $value["pcolor"];
                $dcandidates[$counter]["image"] = $value["image"];
                $counter++;
            }

            // get all casted votes
            $allvotes = array();
            $conter = 0;
            foreach ($dcandidates as $key => $value) {
                // echo $value."~";
                $dvotes = self::getvotes($value["pid"]);
                if (!empty(unserialize($dvotes))) {
                    $dcandid = unserialize($dvotes);
                    $allvotes[$conter]["candid"] = $value["pid"];
                    $allvotes[$conter]["candidname"] = $value["pname"];
                    $allvotes[$conter]["candidcolor"] = $value["pcolor"];
                    $allvotes[$conter]["image"] = $value["image"];
                    $totalvotes = 0;
                    foreach (unserialize($dcandid["casts"]) as $key => $value) {
                        $totalvotes += ($value == "" ? 0 : $value);
                    }
                    $allvotes[$conter]["votes"] = $totalvotes;
                } else {
                    $allvotes[$conter]["candid"] = $value["pid"];
                    $allvotes[$conter]["candidname"] = $value["pname"];
                    $allvotes[$conter]["candidcolor"] = $value["pcolor"];
                    $allvotes[$conter]["image"] = $value["image"];
                    $allvotes[$conter]["votes"] = 0;
                }
                $conter++;
            }

            return serialize($allvotes);
        }
    }


?>
