<?php
    class position
    {
        function __construct() { }
        // general connector
        public static function connectMe()
        {
            $connect = mysqli_connect('localhost', DB_USER, DB_PASSWORD, DB_TABLE);
            if(!$connect) { echo 'Cant connet to DB'; die; } else {
				return $connect;
			}
        }

        public function savePosition($name, $desc)
        {
            $conns = self::connectMe();
            mysqli_query($conns, "insert into positions (posname, posdesc) values ('".$name."', '".$desc."')");
            header("Location: ".HOST."/?page=position&type=all");
        }

        public function saveSinglePosition($id, $name, $desc)
        {
            $conns = self::connectMe();
            mysqli_query($conns, "update positions set posname = '".$name."', posdesc = '".$desc."' where posid = ".$id);
            header("Location: ".HOST."/?page=position&type=all");
        }

        public function getAllPositions()
        {
            $dpositions = array();
            $conns = self::connectMe();
            $getPositions = mysqli_query($conns, "select * from positions");
            $counter = 0;
            while ($row = mysqli_fetch_assoc($getPositions)) {
                $dpositions[$counter]["posid"] = $row["posid"];
                $dpositions[$counter]["posname"] = $row["posname"];
                $dpositions[$counter]["posdesc"] = $row["posdesc"];
                $counter++;
            }
            return serialize($dpositions);
        }
        public function getOnePosition($id)
        {
            $dpositions = array();
            $conns = self::connectMe();
            $getPositions = mysqli_query($conns, "select * from positions where posid = ".$id);
            while ($row = mysqli_fetch_assoc($getPositions)) {
                $dpositions["posid"] = $row["posid"];
                $dpositions["posname"] = $row["posname"];
                $dpositions["posdesc"] = $row["posdesc"];
            }
            return serialize($dpositions);
        }
        public function getPositionName($id)
        {
            $conns = self::connectMe();
            $getPositions = mysqli_query($conns, "select * from positions where posid = ".$id);
            $posname = "";
            while ($row = mysqli_fetch_assoc($getPositions)) {  $posname = $row["posname"]; }
            return $posname;
        }
        public function delete($id)
        {
            $conns = self::connectMe();
            mysqli_query($conns, "delete from positions where posid = ".$id);
            header("Location: ".HOST."/?page=position&type=all");
        }
    }
?>
