<?php

    /**
     *
     */
    class area
    {
        function __construct()
        { }
        public static function connectMe()
        {
            $connect = mysqli_connect('localhost', DB_USER, DB_PASSWORD, DB_TABLE);
            if(!$connect) { echo 'Cant connet to DB'; die; } else {
				return $connect;
			}
        }
        public function addArea($slug, $name, $desc)
        {
            $conns = self::connectMe();
            mysqli_query($conns, "insert into area (aname, alocation, aslug) values ('".$name."', '".$desc."', '".$slug."')");
            header("Location: ".HOST."/?page=area&type=all");
        }
        public function getAllArea()
        {
            $dareas = array();
            $conns = self::connectMe();
            $getPositions = mysqli_query($conns, "select * from area");
            $counter = 0;
            while ($row = mysqli_fetch_assoc($getPositions)) {
                $dareas[$counter]["aid"] = $row["aid"];
                $dareas[$counter]["aslug"] = $row["aslug"];
                $dareas[$counter]["aname"] = $row["aname"];
                $dareas[$counter]["alocation"] = $row["alocation"];
                $counter++;
            }
            return serialize($dareas);
        }
        public function getAllAreabyid($id)
        {
            $dareas = array();
            $conns = self::connectMe();
            $getPositions = mysqli_query($conns, "select * from area where aid = ".$id);
            while ($row = mysqli_fetch_assoc($getPositions)) {
                $dareas["aid"] = $row["aid"];
                $dareas["aslug"] = $row["aslug"];
                $dareas["aname"] = $row["aname"];
                $dareas["alocation"] = $row["alocation"];
            }
            return serialize($dareas);
        }
        public function updatearea($id, $slug, $name, $desc)
        {
            $conns = self::connectMe();
            mysqli_query($conns, "update area set aname = '".$name."', aslug = '".$slug."', alocation = '".$desc."' where aid = ".$id);
            header("Location: ".HOST."/?page=area&type=all");
        }
        public function delete($id)
        {
            $conns = self::connectMe();
            mysqli_query($conns, "delete from area where aid = ".$id);
            header("Location: ".HOST."/?page=area&type=all");
        }
    }


?>
