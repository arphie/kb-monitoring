<?php
    session_start();
    ob_start();
    ini_set('display_errors', 1);
    error_reporting(E_ALL);
?>
<?php include 'classes/position.php'; ?>
<?php include 'classes/candidates.php'; ?>
<?php include 'classes/area.php'; ?>
<?php include 'classes/monitor.php'; ?>
<?php include 'classes/users.php'; ?>
<?php include 'classes/authenticate.php'; ?>
<?php
    $position = new position();
    $candidates = new candidates();
    $area = new area();
    $monitor = new monitor();
    $users = new users();
    $authenticate = new authenticate();
?>
