<?php if( isset($_GET['pos']) && $_GET['pos'] != ""): ?>
<?php
    if(isset($_POST["addpos"])){
        $dareas = array();
        foreach ($_POST as $key => $value):
            if($key != "did" && $key != "addpos"):
                $dareas[$key] = $value;
            endif;
        endforeach;
        $monitor->castVote($_POST["did"], serialize($dareas));

    }
?>

<?php $candidname = $candidates->getCandidateByID($_GET["pos"]); ?>
<?php $candiddetails = unserialize($candidname); ?>
<?php $position = $position->getPositionName($candiddetails["ppos"]); ?>
<?php $dvoterdetails = unserialize($monitor->getvotes($_GET["pos"])); ?>
<?php if($monitor->checkifvoteiscasted($_GET["pos"])): ?>
<?php $dvotes = unserialize($dvoterdetails["casts"]); ?>
<?php endif; ?>


<h3 class="page-title"><?php echo $candiddetails["pname"]; ?> <small>for <?php echo $position; ?></small></h3>
<div class="maincontainer">
    <form class="" action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
        <div class="form-body">
            <?php $dareas = $area->getAllArea(); ?>


            <?php foreach (unserialize($dareas) as $key => $value): ?>
                <div class="col-md-2">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><?php echo $value["aname"]; ?></h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group form-md-line-input has-info" style="margin-bottom:0;">
                                <input type="text" class="form-control" id="candname" placeholder="Enter Votes" name="<?php echo $value["aslug"]; ?>" value="<?php echo @$dvotes[$value["aslug"]]; ?>">
                                <!-- <label for="candname"><?php echo $value["aname"]; ?></label> -->
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
            <br class="clear">
            <div class="form-group form-md-line-input">
                <input type="hidden" name="did" value="<?php echo $candiddetails["pid"]; ?>">
                <input type="submit" name="addpos" value="Save">
			</div>
        </div>
    </form>
</div>
<?php else: ?>
    <div class="note note-danger">
        <h4 class="block">Item connot be found!</h4>
        <p>the item that you are trying to access does not exist in our database</p>
    </div>
<?php endif; ?>
