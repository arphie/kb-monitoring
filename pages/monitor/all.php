<?php
    $allpositions = $position->getAllPositions();
?>
<h2 class="page-title">Monitoring</h2>
<?php foreach (unserialize($allpositions) as $key => $value):?>
    <?php $dcandidates = $monitor->getCandidbyPosition($value["posid"]); ?>
    <div class="portlet <?php echo (!empty(unserialize($dcandidates)) ? "blue" : "red"); ?> box">
    	<div class="portlet-title">
    		<div class="caption">
    			<i class="fa fa-list"></i><?php echo $value["posname"]; ?> <?php echo (!empty(unserialize($dcandidates)) ? "" : '<span style="font-size:12px;">(no candidate yet)</span>'); ?>
    		</div>
    		<div class="tools">
    			<a href="javascript:;" class="expand" data-original-title="" title=""> </a>
    		</div>
    	</div>
    	<div class="portlet-body" style="display:none;">
            <div class="note note-success">
				<p><?php echo $value["posdesc"]; ?></p>
			</div>
            <?php if (!empty(unserialize($dcandidates))) : ?>
                <?php $dcandidates = unserialize($dcandidates); ?>
                <ul class="candidlist">
                    <?php foreach ($dcandidates as $key => $value): ?>
                        <li style="background:<?php echo $value["pcolor"]; ?>;"><a href="<?php echo HOST; ?>/?page=monitor&type=cast&cid=<?php echo $value["pid"]; ?>"><i style="color:<?php echo $value["pcolor"]; ?>;" class="fa <?php echo ($value["pcolor"] == "#000000" ? "fa-heart-o" : "fa-heart"); ?>"></i> <?php echo $value["pname"]; ?></a></li>
                    <?php endforeach; ?>
                </ul>
            <?php else: ?>
                <div class="btn-group">
                    <a href="/emonitor/?page=candidate&amp;type=add" class="btn green">Add New <i class="fa fa-plus"></i></a>
                </div>
            <?php endif; ?>
    	</div>
    </div>
<?php endforeach; ?>
