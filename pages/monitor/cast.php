<?php if( isset($_GET['pos']) && $_GET['pos'] != ""): ?>
<?php $allcandidates = $monitor->getCandidbyPosition($_GET["pos"]); ?>
<div class="portlet box blue-steel">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-magic"></i> All Candidates for <?php echo $position->getPositionName($_GET["pos"]); ?>
        </div>
    </div>
    <div class="portlet-body">
        <table class="table table-striped table-bordered table-hover" id="sample_1">
            <thead>
                <tr>
                    <th style="width:50px;text-align:center;">&nbsp;</th>
                    <th style="width:400px;text-align:center;">Name</th>
                    <th style="text-align:center;">Position</th>
                    <th style="width:150px;text-align:center;">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach (unserialize($allcandidates) as $key => $value): ?>
                    <tr class="gradeX <?php echo ($key % 2 == 0 ? "even" : "odd"); ?>" role="row">
                        <td style="text-align:center;">
                            <div ><i style="font-size: 30px;margin-top: 8px;color:<?php echo $value["pcolor"]; ?>;" class="glyphicon glyphicon-fire"></i></div>
                        </td>
                        <td style="text-align:center;vertical-align:middle;"><?php echo $value["pname"]; ?></td>
                        <td style="text-align:center;vertical-align:middle;"><?php echo $position->getPositionName($value["ppos"]); ?></td>
                        <td>
                            <a class="btn purple-studio" title="Cast Vote" href="<?php echo HOST; ?>/?page=monitor&type=vote&pos=<?php echo $value["pid"]; ?>" data-toggle="modal"><i class="icon-note"></i></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<?php else: ?>
    <div class="note note-danger">
        <h4 class="block">Item connot be found!</h4>
        <p>the item that you are trying to access does not exist in our database</p>
    </div>
<?php endif; ?>
