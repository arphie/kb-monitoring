<?php

    if (isset($_POST["addpos"])) {
        $area->updatearea($_POST["did"], $_POST["loctag"],$_POST["locname"],$_POST["locdesc"]);
    }

    $darea = unserialize($area->getAllAreabyid($_GET["aid"]));
?>

<div class="maincontainer">
    <h2 class="page-title">Add Location</h2>
    <form class="" action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
        <div class="form-body">
            <div class="col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Location Details</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group form-md-line-input">
            				<input type="text" class="form-control" id="candname" placeholder="Location Slug" name="loctag" value="<?php echo $darea["aslug"]; ?>" required>
            				<label for="candname">Location Slug</label>
            			</div>
                        <div class="form-group form-md-line-input">
            				<input type="text" class="form-control" id="candname" placeholder="Location Label" name="locname" value="<?php echo $darea["aname"]; ?>" required>
            				<label for="candname">Location Name</label>
            			</div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Location Description</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group form-md-line-input">
            				<textarea class="form-control" rows="3" placeholder="Enter Description" name="locdesc"><?php echo $darea["alocation"]; ?></textarea>
            				<label for="form_control_1">Location Description</label>
            			</div>
                    </div>
                </div>
            </div>
            <br class="clear">
            <div class="form-group form-md-line-input">
                <input type="hidden" name="did" value="<?php echo $_GET["aid"]; ?>">
                <input type="submit" name="addpos" value="Save">
			</div>
        </div>
    </form>
</div>
