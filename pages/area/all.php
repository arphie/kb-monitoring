<?php $dcandidates = unserialize($area->getAllArea()); ?>
<?php
    if(isset($_GET["action"]) && $_GET["action"] == "delete"){
        $area->delete($_GET["cid"]);
    }
?>
<div class="portlet box blue-steel">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-magic"></i> All Areas
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <a href="<?php echo HOST; ?>/?page=area&type=add" class="btn green">Add New <i class="fa fa-plus"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover" id="sample_1">
            <thead>
                <tr>
                    <th style="width:5px;text-align:center;">&nbsp;</th>
                    <th style="width:80px;text-align:center;">Slug</th>
                    <th style="text-align:center;">Location</th>
                    <th style="text-align:center;">Description</th>
                    <th style="width:150px;text-align:center;">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($dcandidates as $key => $value): ?>
                    <tr class="gradeX <?php echo ($key % 2 == 0 ? "even" : "odd"); ?>" role="row">
                        <td style="text-align:center;vertical-align:middle;">&nbsp;</td>
                        <td style="text-align:center;vertical-align:middle;"><?php echo $value["aslug"]; ?></td>
                        <td style="text-align:center;vertical-align:middle;"><?php echo $value["aname"]; ?></td>
                        <td style="text-align:center;vertical-align:middle;"><?php echo $value["alocation"]; ?></td>
                        <td>
                            <a class="btn purple-studio" title="Edit Position" href="<?php echo HOST; ?>/?page=area&type=edit&aid=<?php echo $value["aid"]; ?>" data-toggle="modal"><i class="icon-pencil"></i></a>
                            <!-- <a class="btn red-sunglo" title="Delete" href="#"><i class="icon-trash"></i></a> -->
                            <button id="<?php echo $value["aid"]; ?>" class="btn btn-large btn-danger deleteme" data-toggle="confirmation" data-original-title="Are you sure ?" title=""><i class="icon-trash"></i></button>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
