<?php
    if (isset($_POST["addpos"])) {
        $theimagelink = "";
        if($_FILES["dupload"]["type"] != ""){
            $theimagelink = $candidates->uploadimage($_FILES["dupload"]);
        } else {
            $theimagelink = $_POST["oldphoto"];
        }
        $status = $candidates->updateProfile($_POST, $theimagelink);
    }

?>
<div class="maincontainer">
    <?php if(isset($_GET['cid']) && $_GET['cid'] != ""): ?>
        <?php $dcandidate = unserialize($candidates->getCandidateByID($_GET['cid'])); ?>
        <?php if($_GET['type'] == "prof"){ $dcandidate = unserialize($candidates->getalldata($_GET['cid'])); } ?>
        <!-- <pre>
            <?php print_r($dcandidate); ?>
        </pre> -->
        <?php if(!empty($dcandidate)): ?>
        <?php if(@$status == 1): ?>
            <div class="note note-success">
				<h4 class="block">Success! </h4>
				<p>This profile has been successfully updated</p>
			</div>
        <?php endif; ?>
        <h2 class="page-title">Edit Profile</h2>
        <form id="profform" class="" action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post" enctype="multipart/form-data">
            <div class="form-body dprofstyle">
                <div class="col-md-12">

                    <div class="col-md-4">
                        <div class="panel panel-primary">
							<div class="panel-heading">
								<h3 class="panel-title">Profile Image</h3>
							</div>
							<div class="panel-body">
                                <?php if($dcandidate["image"] != ""): ?>
                                    <div class="candidpic">
        								<img src="<?php echo $dcandidate["image"]; ?>" class="img-responsive" alt="">
                                        <input type="hidden" name="oldphoto" value="<?php echo $dcandidate["image"]; ?>">
        							</div>
                                <?php else: ?>
                                    <input type="hidden" name="oldphoto" value="">
                                <?php endif; ?>
                                <div class="form-group duploadpart" style="margin-top: 15px;border: 1px solid #E5E5E5;border-radius: 3px;padding: 5px 15px;">
                                    <label>File input</label>
                                    <input type="file" id="dupload" name="dupload">
                                    <p class="help-block">Upload jpeg, jpg or png files only</p>
                                </div>
							</div>
						</div>
                        <div class="panel panel-primary">
							<div class="panel-heading">
								<h3 class="panel-title">Profile Details</h3>
							</div>
							<div class="panel-body">
                                <?php $ddate = explode("/", $dcandidate["bdate"]); ?>
                                <div class="form-group form-md-line-input has-info">
                                    <input type="text" class="form-control" id="form_control_1" placeholder="Enter your First Name" name="fname" value="<?php echo @$dcandidate["fname"]; ?>">
                                    <label for="form_control_1">First Name</label>
                                    <!-- <span class="help-block">Some help goes here...</span> -->
                                </div>
                                <div class="form-group form-md-line-input has-info">
                                    <input type="text" class="form-control" id="form_control_1" placeholder="Enter your Nickname" name="nname" value="<?php echo @$dcandidate["nname"]; ?>">
                                    <label for="form_control_1">Nickname</label>
                                    <!-- <span class="help-block">Some help goes here...</span> -->
                                </div>
                                <div class="form-group form-md-line-input has-info">
                                    <input type="text" class="form-control" id="form_control_1" placeholder="Enter your Last Name" name="lname" value="<?php echo @$dcandidate["lname"]; ?>">
                                    <label for="form_control_1">Last Name</label>
                                    <!-- <span class="help-block">Some help goes here...</span> -->
                                </div>
                                <h3>Birth Date</h3>
                                <div class="form-group form-md-line-input has-info has-info">
                                    <select class="form-control" id="form_control_month" name="bmonth">
                                        <option value="">Select Month</option>
                                        <option value="1" <?php echo ($ddate["0"] == "1" ? "selected" : "" ); ?>>January</option>
                                        <option value="2" <?php echo ($ddate["0"] == "2" ? "selected" : "" ); ?>>February</option>
                                        <option value="3" <?php echo ($ddate["0"] == "3" ? "selected" : "" ); ?>>March</option>
                                        <option value="4" <?php echo ($ddate["0"] == "4" ? "selected" : "" ); ?>>April</option>
                                        <option value="5" <?php echo ($ddate["0"] == "5" ? "selected" : "" ); ?>>May</option>
                                        <option value="6" <?php echo ($ddate["0"] == "6" ? "selected" : "" ); ?>>June</option>
                                        <option value="7" <?php echo ($ddate["0"] == "7" ? "selected" : "" ); ?>>July</option>
                                        <option value="8" <?php echo ($ddate["0"] == "8" ? "selected" : "" ); ?>>August</option>
                                        <option value="9" <?php echo ($ddate["0"] == "9" ? "selected" : "" ); ?>>September</option>
                                        <option value="10" <?php echo ($ddate["0"] == "10" ? "selected" : "" ); ?>>October</option>
                                        <option value="11" <?php echo ($ddate["0"] == "11" ? "selected" : "" ); ?>>November</option>
                                        <option value="12" <?php echo ($ddate["0"] == "12" ? "selected" : "" ); ?>>December</option>
                                    </select>
                                    <label for="form_control_month">Month</label>
                                </div>
                                <div class="form-group form-md-line-input has-info has-info">
                                    <select class="form-control" id="form_control_date" name="bdate">
                                        <option value="">Select Date</option>
                                        <?php for( $counter = 1; $counter <= 31; $counter++ ): ?>
                                            <option value="<?php echo $counter; ?>" <?php echo (@$ddate["1"] == $counter ? "selected" : "" ); ?>><?php echo $counter; ?></option>
                                        <?php endfor; ?>
                                    </select>
                                    <label for="form_control_date">Date</label>
                                </div>
                                <div class="form-group form-md-line-input has-info has-info">
                                    <select class="form-control" id="form_control_year" name="byear">
                                        <option value="">Select Year</option>
                                        <?php
                                            $current_year = 2016;
                                            $base_year = $current_year - 18;
                                        ?>
                                        <?php for( $counter = 1; $counter <= 90; $counter++ ): ?>
                                            <?php $dyear = $base_year--; ?>
                                                <option value="<?php echo $dyear; ?>" <?php echo (@$ddate["2"] == $dyear ? "selected" : "" ); ?>><?php echo $dyear; ?></option>
                                        <?php endfor; ?>
                                    </select>
                                    <label for="form_control_year">Year</label>
                                </div>
                                <div class="form-group form-md-line-input has-info">
                                    <textarea class="form-control" rows="3" placeholder="House number, Street / Barangay / Poblacion/Municipality / City" name="address"><?php echo @$dcandidate["address"]; ?></textarea>
                                    <label for="form_control_1">Address</label>
                                </div>
							</div>
						</div>
                    </div>
                    <div class="col-md-8">
                        <div class="panel panel-primary">
							<div class="panel-heading">
								<h3 class="panel-title">About Me</h3>
							</div>
							<div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group form-md-line-input has-info">
                                        <label for="form_control_1">I am..</label>
                                        <div name="about" id="dabout"><?php echo @$dcandidate["about"]; ?></div>
                                        <input id="profabout" type="hidden" name="about">
        								<!-- <textarea class="form-control" rows="3" placeholder="" name="about"><?php echo @$dcandidate["about"]; ?></textarea> -->
        							</div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group form-md-line-input has-info">
                                        <label for="form_control_1">Platforms</label>
                                        <div name="goal" id="dgoal"><?php echo @$dcandidate["goal"]; ?></div>
                                        <input id="profgoal" type="hidden" name="goal">
        								<!-- <textarea class="form-control" rows="3" placeholder="" name="goal"><?php echo @$dcandidate["goal"]; ?></textarea> -->
        							</div>
                                </div>
                                <br class="clear">
							</div>
						</div>
                        <br class="clear">
                    </div>
                </div>
                <br style="clear:both;">
                <div class="form-group form-md-line-input has-info">
                    <input type="hidden" name="did" value="<?php echo $_GET["cid"]; ?>">
                    <input type="submit" name="addpos" value="Save">
    			</div>
            </div>
        </form>
    <?php else: ?>
        <div class="note note-danger">
            <h4 class="block">Item connot be found!</h4>
            <p>the item that you are trying to access does not exist in our database</p>
        </div>
    <?php endif; ?>
    <?php else: ?>
        <div class="note note-danger">
            <h4 class="block">Item connot be found!</h4>
            <p>the item that you are trying to access does not exist in our database</p>
        </div>
    <?php endif; ?>
</div>
