<?php
    if (isset($_POST["addpos"])) {
        $candidates->insertCandidate($_POST["dcanname"], $_POST["dcanpos"], $_POST["dcancolor"]);
    }

    $allpositions = unserialize($position->getAllPositions());
?>
<div class="maincontainer">
    <h2 class="page-title">Add Candidate</h2>
    <form class="" action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
        <div class="form-body">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Candidate info</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group form-md-line-input">
            				<input type="text" class="form-control" id="candname" placeholder="Enter Candidate" name="dcanname" required>
            				<label for="candname">Candidate Name</label>
            			</div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Candidate Color</h3>
                    </div>
                    <div class="panel-body">
                        <label for="candipos" style="font-size: 13px; color: #888888;">Pick color for the Candidate</label>
                        <div class="input-group color colorpicker-default" data-color="#000000" data-color-format="rgba">
                            <input type="text" class="form-control" value="#000000" name="dcancolor" readonly>
                            <span class="input-group-btn">
                            <button class="btn default" type="button"><i style="background-color: #3865a8;"></i>&nbsp;</button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Desired Position</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group form-md-line-input has-info">
                            <select class="form-control" id="candipos" name="dcanpos" required>
                                <option value=""></option>
                                <?php foreach ($allpositions as $key => $value): ?>
                                    <option value="<?php echo $value["posid"]?>"><?php echo $value["posname"]?></option>
                                <?php endforeach; ?>
                            </select>
                            <label for="candipos">Available Position</label>
                        </div>
                    </div>
                </div>
            </div>
            <br style="clear:both;">
            <div class="form-group form-md-line-input">
                <input type="submit" name="addpos" value="Save">
            </div>
        </div>
    </form>
</div>
