<?php $dcandidates = unserialize($candidates->getAllCandidates()); ?>
<?php
    if(isset($_GET["action"]) && $_GET["action"] == "delete"){
        $candidates->delete($_GET["cid"]);
    }
?>
<div class="portlet box blue-steel">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-magic"></i> All Candidates
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <a href="<?php echo HOST; ?>/?page=candidate&type=add" class="btn green">Add New <i class="fa fa-plus"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover" id="sample_1">
            <thead>
                <tr>
                    <th style="width:50px;text-align:center;">&nbsp;</th>
                    <th style="width:400px;text-align:center;">Name</th>
                    <th style="text-align:center;">Position</th>
                    <th style="width:200px;text-align:center;">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($dcandidates as $key => $value): ?>
                    <tr class="gradeX <?php echo ($key % 2 == 0 ? "even" : "odd"); ?>" role="row">
                        <td style="text-align:center;">
                            <div ><i style="font-size: 30px;margin-top: 8px;color:<?php echo $value["pcolor"]; ?>;" class="glyphicon glyphicon-fire"></i></div>
                        </td>
                        <td style="text-align:center;vertical-align:middle;"><?php echo $value["pname"]; ?></td>
                        <td style="text-align:center;vertical-align:middle;"><?php echo $position->getPositionName($value["ppos"]); ?></td>
                        <td>
                            <a class="btn purple-studio" title="Edit Position" href="<?php echo HOST; ?>/?page=candidate&type=edit&cid=<?php echo $value["pid"]; ?>" data-toggle="modal"><i class="icon-pencil"></i></a>
                            <a class="btn purple-studio" title="Edit Profile" href="<?php echo HOST; ?>/?page=candidate&type=prof&cid=<?php echo $value["pid"]; ?>" data-toggle="modal"><i class="icon-user-follow"></i></a>
                            <!-- <a class="btn red-sunglo" title="Delete" href="<?php echo HOST; ?>/?page=candidate&type=all&action=delete&cid=<?php echo $value["pid"]; ?>"><i class="icon-trash"></i></a> -->
                            <button id="<?php echo $value["pid"]; ?>" class="btn btn-large btn-danger deleteme" data-toggle="confirmation" data-original-title="Are you sure ?" title=""><i class="icon-trash"></i></button>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
