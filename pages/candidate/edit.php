<?php
    if (isset($_POST["addpos"])) {
        $candidates->updateCandidate($_POST["did"], $_POST["dcanname"], $_POST["dcanpos"], $_POST["dcancolor"]);
    }

    $allpositions = unserialize($position->getAllPositions());
?>
<div class="maincontainer">
    <?php if(isset($_GET['cid']) && $_GET['cid'] != ""): ?>
        <?php $dcandidate = unserialize($candidates->getCandidateByID($_GET['cid'])); ?>
        <?php if(!empty($dcandidate)): ?>
        <h2 class="page-title">Edit Candidate</h2>
        <form class="" action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
            <div class="form-body">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Candidate info</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group form-md-line-input">
                				<input type="text" class="form-control" id="candname" placeholder="Enter Candidate" name="dcanname" value="<?php echo $dcandidate["pname"]; ?>" required>
                				<label for="candname">Candidate Name</label>
                			</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Candidate Color</h3>
                        </div>
                        <div class="panel-body">
                            <label for="candipos" style="font-size: 13px; color: #888888;">Pick color for the Candidate</label>
                            <div class="input-group color colorpicker-default" data-color="<?php echo $dcandidate["pcolor"]; ?>" data-color-format="rgba">
                                <input type="text" class="form-control" value="<?php echo $dcandidate["pcolor"]; ?>" name="dcancolor" readonly>
                                <span class="input-group-btn">
                                <button class="btn default" type="button"><i style="background-color: #3865a8;"></i>&nbsp;</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Desired Position</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group form-md-line-input has-info">
                				<select class="form-control" id="candipos" name="dcanpos" required>
                					<option value=""></option>
                                    <?php foreach ($allpositions as $key => $value): ?>
                                        <option value="<?php echo $value["posid"]; ?>" <?php echo ($dcandidate["ppos"] == $value["posid"] ? "selected" : ""); ?>><?php echo $value["posname"]?></option>
                                    <?php endforeach; ?>
                				</select>
                				<label for="candipos">Available Position</label>
                			</div>
                        </div>
                    </div>

                </div>
                <br style="clear:both;">
                <div class="form-group form-md-line-input">
                    <input type="hidden" name="did" value="<?php echo $dcandidate["pid"]; ?>">
                    <input type="submit" name="addpos" value="Save">
    			</div>
            </div>
        </form>
    <?php else: ?>
        <div class="note note-danger">
            <h4 class="block">Item connot be found!</h4>
            <p>the item that you are trying to access does not exist in our database</p>
        </div>
    <?php endif; ?>
    <?php else: ?>
        <div class="note note-danger">
            <h4 class="block">Item connot be found!</h4>
            <p>the item that you are trying to access does not exist in our database</p>
        </div>
    <?php endif; ?>
</div>
