<?php $dpositions = unserialize($position->getAllPositions()); ?>
<?php
    if(isset($_GET["action"]) && $_GET["action"] == "delete"){
        $position->delete($_GET["cid"]);
    }
?>
<div class="portlet box blue-steel">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-magic"></i> All Positions
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <a href="<?php echo HOST; ?>/?page=position&type=add" class="btn green">Add New <i class="fa fa-plus"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover" id="sample_1">
            <thead>
                <tr>
                    <th style="width:50px;text-align:center;">id</th>
                    <th style="width:200px;text-align:center;">Position</th>
                    <th style="text-align:center;">Description</th>
                    <th style="width:150px;text-align:center;">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($dpositions as $key => $value): ?>
                    <tr class="gradeX <?php echo ($key % 2 == 0 ? "even" : "odd"); ?>" role="row">
                        <td style="text-align:center;"><?php echo $value["posid"]; ?></td>
                        <td style="text-align:center;"><?php echo $value["posname"]; ?></td>
                        <td style="text-align:center;"><?php echo $value["posdesc"]; ?></td>
                        <td>
                            <a class="btn purple-studio" title="Edit Position" href="<?php echo HOST; ?>/?page=position&type=edit&pid=<?php echo $value["posid"]; ?>" data-toggle="modal"><i class="icon-pencil"></i></a>
                            <!-- <a class="btn red-sunglo" title="Delete" href="#"><i class="icon-trash"></i></a> -->
                            <button id="<?php echo $value["posid"]; ?>" class="btn btn-large btn-danger deleteme" data-toggle="confirmation" data-original-title="Are you sure ?" title=""><i class="icon-trash"></i></button>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
