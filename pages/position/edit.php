<?php
    if (isset($_POST["addpos"])) {
        $position->saveSinglePosition($_POST["did"],$_POST["position_name"],$_POST["position_desc"]);
    }
?>
<div class="maincontainer">
    <?php if(isset($_GET['pid']) && $_GET['pid'] != ""): ?>
        <?php
            $dposition = $position->getOnePosition($_GET['pid']);
            $dsingleposition = unserialize($dposition);
        ?>
        <h2 class="page-title">Edit Position</h2>
        <form class="" action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
            <div class="form-body">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Position Details</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group form-md-line-input">
            				<input type="text" class="form-control" id="form_control_1" placeholder="Enter Position" name="position_name" value="<?php echo $dsingleposition["posname"]; ?>" required>
            				<label for="form_control_1">Position Name</label>
            			</div>
                        <div class="form-group form-md-line-input">
            				<textarea class="form-control" rows="3" placeholder="Enter Position Description" name="position_desc" required><?php echo $dsingleposition["posname"]; ?></textarea>
            				<label for="form_control_1">Position Description</label>
            			</div>
                    </div>
                </div>
                <div class="form-group form-md-line-input">
                    <input type="hidden" name="did" value="<?php echo $_GET['pid']; ?>">
                    <input type="submit" name="addpos" value="Save">
    			</div>
            </div>
        </form>
    <?php else: ?>
        <div class="note note-danger">
			<h4 class="block">Item connot be found!</h4>
			<p>the item that you are trying to access does not exist in our database</p>
		</div>
    <?php endif; ?>
</div>
