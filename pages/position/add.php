<?php
    if (isset($_POST["addpos"])) {
        $position->savePosition($_POST["position_name"],$_POST["position_desc"]);
    }
?>
<div class="maincontainer">
    <h2 class="page-title">Add Position</h2>
    <form class="" action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
        <div class="form-body">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Position Details</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group form-md-line-input">
        				<input type="text" class="form-control" id="form_control_1" placeholder="Enter Position" name="position_name" required>
        				<label for="form_control_1">Position Name</label>
        			</div>
                    <div class="form-group form-md-line-input">
        				<textarea class="form-control" rows="3" placeholder="Enter Position Description" name="position_desc" required></textarea>
        				<label for="form_control_1">Position Description</label>
        			</div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <input type="submit" name="addpos" value="Save">
			</div>
        </div>
    </form>
</div>
