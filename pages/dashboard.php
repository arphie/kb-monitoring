<h2 class="page-title">Dashboard</h2>
<?php $allpos = $position->getAllPositions(); ?>
<?php foreach (unserialize($allpos) as $key => $value) :?>
    <div class="theoverviewitem">
        <h4 class="block"><a href="<?php echo HOST; ?>/?page=monitor&type=cast&pos=<?php echo $value["posid"]; ?>" class="btn btn-icon-only btn-circle green" style="height: 25px;width: 25px;padding: 3px;"><i class="fa fa-plus"></i></a> <?php echo $value["posname"]; ?></h4>
        <?php $dmonitoring = $monitor->getTotalVotesPerCandidates($value["posid"]); ?>
        <?php $vtotal = 0; ?>
        <?php foreach (unserialize($dmonitoring) as $key => $value): ?>
            <?php $vtotal += $value["votes"]; ?>
        <?php endforeach; ?>
        <?php if(!empty(unserialize($dmonitoring))): ?>
            <!-- <pre>
                <?php print_r(unserialize($dmonitoring)); ?>
            </pre> -->
            <?php $numofelement = count(unserialize($dmonitoring)); ?>
            <?php //echo $numofelement; ?>
        <div class="progress dmonitorbar <?php echo $vtotal."-".$numofelement; ?>">
            <?php $count = 1; ?>

            <?php foreach (unserialize($dmonitoring) as $key => $value): ?>
                <?php
                    $dwidth = ( $numofelement <= 1 ? "100" : ( $vtotal == 0 ? (100 / $numofelement) : ($value["votes"] <= 0 ? "0" : ($value["votes"]/$vtotal)*100)));
                    $dlabel = ( $value["votes"] <= 0 ? "No votes yet" : number_format(($value["votes"]/$vtotal)*100,2)."%");

                ?>
                <div class="progress-bar progress-bar-success <?php echo ($count % 2 == 0 ? "even" : "odd");?>" style="width: <?php echo $dwidth; ?>%;display: <?php echo ($dwidth == 0 ? "none" : "");?>;">
            		<span class="sr-only"> <?php echo $value["candidname"]; ?> ( <?php echo $dlabel; ?> ) </span>
            	</div>
                <?php $count++; ?>
            <?php endforeach; ?>
        	<!-- <div class="progress-bar progress-bar-warning" style="width: 20%">
        		<span class="sr-only">
        		20% Complete (warning) </span>
        	</div>
        	<div class="progress-bar progress-bar-danger" style="width: 5%">
        		<span class="sr-only">
        		10% Complete (danger) </span>
        	</div> -->
        </div>
    <?php else: ?>
        <div class="alert alert-warning">
			<strong>Please Add a Candidate!</strong> Add a Candidate and cast a vote to have this section working.
		</div>
    <?php endif; ?>
    </div>
<?php endforeach; ?>
