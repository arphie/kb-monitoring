<?php
    if (isset($_POST["addpos"])) {
        $users->savePosition($_POST["username"], $_POST["password"], $_POST["access"]);
    }
?>
<div class="maincontainer">
    <h2 class="page-title">Add User</h2>
    <form class="" action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
        <div class="form-body">
            <div class="form-group form-md-line-input">
				<input type="text" class="form-control" id="form_control_1" placeholder="Enter Username" name="username" required>
				<label for="form_control_1">Username</label>
			</div>
            <div class="form-group form-md-line-input">
				<input type="password" class="form-control" id="form_control_2" placeholder="Enter Password" name="password" required>
				<label for="form_control_2">Password</label>
			</div>
            <div class="form-group form-md-line-input has-info">
				<select class="form-control" id="form_control_1" name="access" required>
					<option value=""></option>
					<option value="1">Super Admin</option>
					<option value="2">Admin</option>
					<option value="3">Viewer</option>
				</select>
				<label for="form_control_1">Access Level</label>
			</div>
            <div class="form-group form-md-line-input">
                <input type="submit" name="addpos" value="Save">
			</div>
        </div>
    </form>
</div>
