<?php
    if (isset($_POST["addpos"])) {
        $users->updateuser($_POST["uid"],$_POST["username"],$_POST["access"]);
    }
?>
<div class="maincontainer">
    <?php if(isset($_GET['pid']) && $_GET['pid'] != ""): ?>
        <?php
            $duser = $users->getuserByID($_GET['pid']);
            $duserinfo = unserialize($duser);
        ?>
        <div class="maincontainer">
            <h2 class="page-title">Add User</h2>
            <form class="" action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
                <div class="form-body">
                    <div class="form-group form-md-line-input">
        				<input type="text" class="form-control" id="form_control_1" placeholder="Enter Position" name="username" value="<?php echo $duserinfo["username"]; ?>" required>
        				<label for="form_control_1">Username</label>
        			</div>
                    <div class="form-group form-md-line-input has-info">
        				<select class="form-control" id="form_control_1" name="access" required>
        					<option value=""></option>
        					<option value="1" <?php echo ($duserinfo["access"] == "1" ? "selected" : ""); ?>>Super Admin</option>
        					<option value="2" <?php echo ($duserinfo["access"] == "2" ? "selected" : ""); ?>>Admin</option>
        					<option value="3" <?php echo ($duserinfo["access"] == "3" ? "selected" : ""); ?>>Viewer</option>
        				</select>
        				<label for="form_control_1">Access Level</label>
        			</div>
                    <div class="form-group form-md-line-input">
                        <input type="hidden" name="uid" value="<?php echo $_GET['pid']; ?>">
                        <input type="submit" name="addpos" value="Save">
        			</div>
                </div>
            </form>
        </div>
    <?php else: ?>
        <div class="note note-danger">
			<h4 class="block">Item connot be found!</h4>
			<p>the item that you are trying to access does not exist in our database</p>
		</div>
    <?php endif; ?>
</div>
