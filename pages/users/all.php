<?php $dpositions = unserialize($users->getAllPositions()); ?>
<?php
    if(isset($_GET["action"]) && $_GET["action"] == "delete"){
        $users->delete($_GET["cid"]);
    }
?>
<div class="portlet box blue-steel">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-magic"></i> All Users
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <a href="<?php echo HOST; ?>/?page=user&type=add" class="btn green">Add New <i class="fa fa-plus"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover" id="sample_1">
            <thead>
                <tr>
                    <th style="width:50px;text-align:center;">id</th>
                    <th style="text-align:center;">Uesrname</th>
                    <th style="text-align:center;">Access</th>
                    <th style="width:200px;text-align:center;">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($dpositions as $key => $value): ?>
                    <tr class="gradeX <?php echo ($key % 2 == 0 ? "even" : "odd"); ?>" role="row">
                        <td style="text-align:center;"><?php echo $value["userid"]; ?></td>
                        <td style="text-align:center;"><?php echo $value["username"]; ?></td>
                        <td style="text-align:center;"><?php echo $users->idtonameaccess($value["access"]); ?></td>
                        <td>
                            <a class="btn blue-chambray" title="Edit Position" href="<?php echo HOST; ?>/?page=user&type=edit&pid=<?php echo $value["userid"]; ?>" data-toggle="modal"><i class="icon-pencil"></i></a>
                            <a class="btn green-seagreen" title="Change Password" href="<?php echo HOST; ?>/?page=user&type=cpass&pid=<?php echo $value["userid"]; ?>" data-toggle="modal"><i class="icon-key"></i></a>
                            <!-- <a class="btn red-sunglo" title="Delete" href="#"><i class="icon-trash"></i></a> -->
                            <button id="<?php echo $value["userid"]; ?>" class="btn btn-large btn-danger red-intense deleteme" data-toggle="confirmation" data-original-title="Are you sure ?" title=""><i class="icon-trash"></i></button>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
