<?php
    if (isset($_POST["addpos"])) {
        if(md5(trim($_POST["npass"])) == md5(trim($_POST["ncpass"]))){
            $users->changepass($_POST["uid"],md5(trim($_POST["npass"])));
        } else {
            ?>
            <div class="alert alert-danger">
                <strong>Error!</strong> Password did not match
            </div>
            <?php
        }
    }
?>
<div class="maincontainer">
    <?php if(isset($_GET['pid']) && $_GET['pid'] != ""): ?>
        <?php
            $duser = $users->getuserByID($_GET['pid']);
            $duserinfo = unserialize($duser);
        ?>
        <div class="maincontainer">
            <h2 class="page-title">Change Password</h2>
            <form class="" action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
                <div class="form-body">
                    <div class="form-group form-md-line-input">
        				<input type="password" class="form-control" id="form_control_1" placeholder="Enter New Password" name="npass" required>
        				<label for="form_control_1">New Password</label>
        			</div>
                    <div class="form-group form-md-line-input">
        				<input type="password" class="form-control" id="form_control_1" placeholder="Confirm New Password" name="ncpass" required>
        				<label for="form_control_1">Confirm Password</label>
        			</div>
                    <div class="form-group form-md-line-input">
                        <input type="hidden" name="uid" value="<?php echo $_GET['pid']; ?>">
                        <input type="submit" name="addpos" value="Save">
        			</div>
                </div>
            </form>
        </div>
    <?php else: ?>
        <div class="note note-danger">
			<h4 class="block">Item connot be found!</h4>
			<p>the item that you are trying to access does not exist in our database</p>
		</div>
    <?php endif; ?>
</div>
