<!-- BEGIN CONTAINER -->
<div class="container">
    <div class="page-container">
    	<?php include 'sidebar.php'; ?>
        <div class="page-content-wrapper">
            <div class="page-content">
                <?php
                    if(isset($_GET['page']) && file_exists("pages/".$_GET['page'].".php")) {
                        include "pages/".$_GET['page'].".php";
                    } elseif(isset($_GET['page']) && !file_exists("pages/".$_GET['page'].".php")) {
                        include 'template/404.php';
                    } else {
                        include 'template/404.php';
                    }
                ?>
            </div>
        </div>

    	<!-- BEGIN CONTENT -->
    </div>
    <!-- END PAGE CONTAINER -->
