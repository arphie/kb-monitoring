<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo HOST; ?>/assets/global/plugins/respond.min.js"></script>
<script src="<?php echo HOST; ?>/assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="<?php echo HOST; ?>/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo HOST; ?>/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?php echo HOST; ?>/assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?php echo HOST; ?>/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo HOST; ?>/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?php echo HOST; ?>/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo HOST; ?>/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo HOST; ?>/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="<?php echo HOST; ?>/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<?php echo HOST; ?>/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->

<script src="<?php echo HOST ?>/assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
<script src="<?php echo HOST ?>/assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
<script src="<?php echo HOST ?>/assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
<script src="<?php echo HOST ?>/assets/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
<script src="<?php echo HOST ?>/assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
<script src="<?php echo HOST ?>/assets/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
<script src="<?php echo HOST ?>/assets/global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
<script src="<?php echo HOST ?>/assets/global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
<script src="<?php echo HOST ?>/assets/global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
<script src="<?php echo HOST ?>/assets/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>

<script src="<?php echo HOST ?>/assets/global/plugins/flot/jquery.flot.min.js"></script>
<script src="<?php echo HOST ?>/assets/global/plugins/flot/jquery.flot.resize.min.js"></script>
<script src="<?php echo HOST ?>/assets/global/plugins/flot/jquery.flot.pie.min.js"></script>
<script src="<?php echo HOST ?>/assets/global/plugins/flot/jquery.flot.stack.min.js"></script>
<script src="<?php echo HOST ?>/assets/global/plugins/flot/jquery.flot.crosshair.min.js"></script>
<script src="<?php echo HOST ?>/assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>

<script src="<?php echo HOST; ?>/assets/global/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
<script src="<?php echo HOST; ?>/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
<script src="<?php echo HOST; ?>/assets/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js" type="text/javascript" ></script>
<!-- IMPORTANT! fullcalendar depends on jquery-ui.min.js for drag & drop support -->
<script src="<?php echo HOST; ?>/assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
<script src="<?php echo HOST; ?>/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
<script src="<?php echo HOST; ?>/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
<script src="<?php echo HOST; ?>/assets/global/plugins/select2/select2.min.js" type="text/javascript"></script>
<script src="<?php echo HOST; ?>/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?php echo HOST; ?>/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo HOST; ?>/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script src="<?php echo HOST; ?>/assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js" type="text/javascript"></script>
<script src="<?php echo HOST; ?>/assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo HOST; ?>/assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="<?php echo HOST; ?>/assets/admin/layout2/scripts/layout.js" type="text/javascript"></script>
<script src="<?php echo HOST; ?>/assets/admin/layout2/scripts/demo.js" type="text/javascript"></script>
<script src="<?php echo HOST; ?>/assets/admin/pages/scripts/index.js" type="text/javascript"></script>
<script src="<?php echo HOST; ?>/assets/admin/pages/scripts/tasks.js" type="text/javascript"></script>
<script src="<?php echo HOST; ?>/assets/admin/pages/scripts/table-managed.js"></script>
<script src="<?php echo HOST; ?>/assets/admin/pages/scripts/charts-amcharts.js"></script>
<script src="<?php echo HOST; ?>/assets/admin/pages/scripts/charts-flotcharts.js"></script>
<script src="<?php echo HOST; ?>/assets/admin/pages/scripts/components-pickers.js"></script>
<script src="<?php echo HOST; ?>/assets/admin/pages/scripts/ui-confirmations.js"></script>
<script src="<?php echo HOST; ?>/assets/admin/pages/scripts/components-editors.js"></script>

<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {
   Metronic.init(); // init metronic core componets
   Layout.init(); // init layout
   Demo.init(); // init demo features
   Index.init();
   Index.initDashboardDaterange();
   Index.initJQVMAP(); // init index page's custom scripts
   Index.initCalendar(); // init index page's custom scripts
   Index.initCharts(); // init index page's custom scripts
   Index.initChat();
   Index.initMiniCharts();
   Tasks.initDashboardWidget();
   TableManaged.init();
   ComponentsPickers.init();
   UIConfirmations.init(); // init page demo
   ComponentsEditors.init();
});
</script>

<script type="text/javascript">
    $( document ).ready(function() {
		//for adding of grades
		$('.addstudents').click(function(){
			var subjectname = $('.subjctname').val();
			var subjectdesc = $('.subjctdesc').val();
			var newform = '<div class="col-md-3">';
			newform += '<div class="form-group form-md-line-input">';
			newform += '<input type="text" class="form-control" id="form_control_1" name="'+subjectname+'">';
			newform += '<label for="form_control_1">'+subjectname+'</label>';
			newform += '<span class="help-block">'+subjectdesc+'</span>';
			newform += '</div>';
			newform += '</div>';
			$('.subjectform').append(newform);
			$('.subjctname').val("");
			$('.subjctdesc').val("");
		});

        $('button.deleteme[data-toggle=confirmation]').on('confirmed.bs.confirmation', function () {
            var did = $(this).attr("id");
            window.location.href = "<?php echo HOST; ?>/?page=<?php echo $_GET["page"]; ?>&type=all&action=delete&cid="+did;
        });

        $('#dabout, #dgoal').summernote({height: 300});
        $("#profform").submit(function(e){
            var sHTML = $('#dabout').code();
            $("#profabout").val(sHTML);

            var sHTML = $('#dgoal').code();
            $("#profgoal").val(sHTML);
        });
    });
</script>


<script type="text/javascript">
	$(document).ready(function() {

		for (var k = 0; k < 6; k++) {
			var newdata = [];
			series = 2;
			for (var i = 0; i < series; i++) {
				var nvalue = Math.floor(Math.random() * 100) + 1;
				if( i == 1){
					nvalue = 100 - nvalue;
				}
				newdata[i] = {
					label: ( i == 0 ? 'girls' : 'boys'),
					data: nvalue
				};
			}
			if ($('#grade'+(k+1)).size() !== 0) {
				$.plot($("#grade"+(k+1)), newdata, {
					series: {
						pie: {
							innerRadius: 0.5,
							show: true
						}
					}
				});
			}
		}



		AmCharts.makeChart("chart_5", {
			"theme": "light",
			"type": "serial",
			"startDuration": 2,

			"fontFamily": 'Open Sans',

			"color":    '#888',

			"dataProvider": [{
				"country": "Grade 1",
				"visits": 156,
				"color": "#FF0F00"
			}, {
				"country": "Grade 2",
				"visits": 120,
				"color": "#FF6600"
			}, {
				"country": "Grade 3",
				"visits": 130,
				"color": "#FF9E01"
			}, {
				"country": "Grade 4",
				"visits": 90,
				"color": "#FCD202"
			}, {
				"country": "Grade 5",
				"visits": 170,
				"color": "#F8FF01"
			}, {
				"country": "Grade 6",
				"visits": 168,
				"color": "#B0DE09"
			}],
			"valueAxes": [{
				"position": "left",
				"axisAlpha": 0,
				"gridAlpha": 0
			}],
			"graphs": [{
				"balloonText": "[[category]]: <b>[[value]]</b>",
				"colorField": "color",
				"fillAlphas": 0.85,
				"lineAlpha": 0.1,
				"type": "column",
				"topRadius": 1,
				"valueField": "visits"
			}],
			"depth3D": 40,
			"angle": 30,
			"chartCursor": {
				"categoryBalloonEnabled": false,
				"cursorAlpha": 0,
				"zoomable": false
			},
			"categoryField": "country",
			"categoryAxis": {
				"gridPosition": "start",
				"axisAlpha": 0,
				"gridAlpha": 0

			},
			"exportConfig": {
				"menuTop": "20px",
				"menuRight": "20px",
				"menuItems": [{
					"icon": '/lib/3/images/export.png',
					"format": 'png'
				}]
			}
		}, 0);
	});
</script>
