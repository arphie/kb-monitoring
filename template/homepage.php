
<!DOCTYPE html>
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>KB | Election Monitoring</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
<link href="<?php echo HOST; ?>/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo HOST; ?>/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo HOST; ?>/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo HOST; ?>/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
<link href="<?php echo HOST; ?>/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo HOST; ?>/assets/admin/pages/css/profile.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo HOST; ?>/assets/admin/pages/css/timeline.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="<?php echo HOST; ?>/assets/global/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="<?php echo HOST; ?>/assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo HOST; ?>/assets/admin/layout2/css/layout.css" rel="stylesheet" type="text/css"/>
<link id="style_color" href="<?php echo HOST; ?>/assets/admin/layout2/css/themes/grey.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo HOST; ?>/assets/admin/layout2/css/custom.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="favicon.ico"/>
<style media="screen">
    .page-content-wrapper .page-content {
        margin-left: 0;
    }
    .page-header-fixed .page-container {
        margin-top: 35px;
    }
</style>
</head>
<!-- END HEAD -->
<body class="homepage page-boxed page-header-fixed page-container-bg-solid page-sidebar-closed-hide-logo homepage">
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
	<!-- BEGIN HEADER INNER -->
	<!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>
<div class="container">
	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->
		<!-- END SIDEBAR -->
		<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
			<div class="page-content">
				<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
				<!-- /.modal -->
				<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
				<!-- BEGIN PAGE HEADER-->
                <div class="themaincontetn">
                    <div class="col-md-6 thelogo">
                        <h3 class="page-title" style="color: #2991D9;"><span>Kusog</span>Baryohanon</h3>
                    </div>
                    <div class="col-md-6 thehomeright">
                        <a href="<?php echo HOST; ?>/" class="btn btn-icon-only btn-circle green-meadow" title="Button View"><i class="fa fa-desktop"></i></a>
                        <!-- <a href="<?php echo HOST; ?>/?section=stats" class="btn btn-icon-only btn-circle blue" title="Statistics"><i class="fa fa-sliders"></i></a> -->
                        <a href="<?php echo HOST; ?>/?page=logout&type=view" class="btn btn-icon-only btn-circle red-flamingo" title="Sign Out"><i class="fa fa-power-off"></i></a>
                    </div>
                    <br class="clear">
                </div>

                <?php $dposition = $position->getAllPositions(); ?>
                <!-- <ul>
                    <?php foreach (unserialize($position->getAllPositions()) as $key => $value): ?>
                        <li><a href="<?php echo HOST; ?>/?section=positions&posid=<?php echo $value["posid"]; ?>"><?php echo $value["posname"]; ?></a></li>
                    <?php endforeach; ?>
                </ul> -->
                <div class="row">
					<div class="col-md-12">
						<!-- BEGIN ROW -->
						<div class="row">
							<div class="col-md-12">
                                <?php if (@$_GET["section"] == "positions" && isset($_GET["posid"])):?>
                                    <div class="col-md-12">
                                    <?php $dmonitoring = $monitor->getTotalVotesPerCandidates($_GET["posid"]); ?>
                                    <?php if(!empty(unserialize($dmonitoring))): ?>
                                    <div class="portlet light">
                                        <div class="portlet-title">
                                            <div class="caption caption-md">
                                                <i class="icon-bar-chart theme-font hide"></i>
                                                <span class="caption-subject font-blue-madison bold uppercase"><?php echo $position->getPositionName($_GET["posid"]); ?></span>
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <div id="homechart" class="chart" style="height: 400px;">
                                            </div>
                                        </div>
                                    </div>
                                    <?php else: ?>
                                        <div class="note note-info">
            								<h4 class="block">No Candidates Found</h4>
            								<p>Please contact the administrator</p>
            							</div>
                                    <?php endif; ?>
                                    </div>
                                <?php elseif(@$_GET["section"] == "stats" && isset($_GET["posid"])): ?>
                                    <?php $dmonitoring = $monitor->getTotalVotesPerCandidates($_GET["posid"]); ?>
                                    <div class="col-md-12">
                                        <div class="portlet light">
                                            <div class="portlet-title">
                                                <div class="caption caption-md">
                                                    <i class="icon-bar-chart theme-font hide"></i>
                                                    <span class="caption-subject font-blue-madison bold uppercase"><?php echo $position->getPositionName($_GET["posid"]); ?></span>
                                                </div>
                                            </div>
                                            <div class="portlet-body">

                                                <?php foreach (unserialize($dmonitoring) as $key => $value): ?>
                                                    <?php $dmonitor = $monitor->getvotes($value["candid"]); ?>
                                                    <div class="row number-stats margin-bottom-30" style="">
                                                        <div class="stat-right">
                                                            <div class="stat-chart">
                                                                <div id="postat_<?php echo $value["candid"]; ?>"></div>
                                                            </div>
                                                            <div class="stat-number">
                                                                <div class="title"><a href="<?php echo HOST; ?>/?section=profile&posid=<?php echo $value["candid"]; ?>"><?php echo $value["candidname"]; ?></a></div>
                                                                <div class="number"><?php echo $value["votes"]; ?> Votes</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php endforeach; ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php elseif(@$_GET["section"] == "profile" && isset($_GET["posid"])): ?>
                                    <?php $dcandidate = unserialize($candidates->getalldata($_GET["posid"])); ?>
                                    <!-- <pre>
                                        <?php print_r($dcandidate); ?>
                                    </pre> -->
                                    <div class="col-md-12">
                    					<!-- BEGIN PROFILE SIDEBAR -->
                    					<div class="profile-sidebar">
                    						<!-- PORTLET MAIN -->
                    						<div class="portlet light profile-sidebar-portlet">
                    							<!-- SIDEBAR USERPIC -->
                    							<div class="profile-userpic">
                    								<img src="<?php echo ($dcandidate["image"] != "" ? $dcandidate["image"] : HOST."/uploads/default.png"); ?>" class="img-responsive" alt="">
                    							</div>
                    							<!-- END SIDEBAR USERPIC -->
                    							<!-- SIDEBAR USER TITLE -->
                    							<div class="profile-usertitle">
                    								<div class="profile-usertitle-name" style="text-transform:uppercase;">"<?php echo ( $dcandidate["nname"] != "" ? $dcandidate["nname"] : $candidates->getCandidateNameByID($_GET["posid"])); ?>"</div>
                    								<div class="profile-usertitle-job"><?php echo $dcandidate["fname"]." ".$dcandidate["lname"]; ?></div>
                    							</div>
                    							<!-- END SIDEBAR USER TITLE -->
                    							<!-- SIDEBAR BUTTONS -->
                    							<div class="profile-userbuttons">
                    								<!-- <button type="button" class="btn btn-circle green-haze btn-sm">Follow</button>
                    								<button type="button" class="btn btn-circle btn-danger btn-sm">Message</button> -->
                                                    <?php $ixposition = $monitor->getPositonbyCandidate($_GET["posid"]); ?>
                                                    <a href="<?php echo HOST; ?>/?section=positions&posid=<?php echo $ixposition; ?>" class="btn btn-icon-only btn-circle green-meadow" title="View by Chart"><i class="fa fa-bars"></i></a>
                                                    <a href="<?php echo HOST; ?>/?section=stats&posid=<?php echo $ixposition; ?>" class="btn btn-icon-only btn-circle blue" title="View by Area"><i class="fa fa-map-marker"></i></a>
                    							</div>
                    							<!-- END SIDEBAR BUTTONS -->
                    							<!-- SIDEBAR MENU -->
                    							<div class="profile-usermenu">
                    								<ul class="nav">
                    									<li class="active">
                                                            <?php if($dcandidate["bdate"] != "" && $dcandidate["bdate"] != "//"): ?>
                                                                <?php
                                                                $ddate = explode("/",$dcandidate["bdate"]);
                                                                $mons = array(
                                                                    1 => "January",
                                                                    2 => "February",
                                                                    3 => "March",
                                                                    4 => "April",
                                                                    5 => "May",
                                                                    6 => "June",
                                                                    7 => "July",
                                                                    8 => "August",
                                                                    9 => "September",
                                                                    10 => "October",
                                                                    11 => "November",
                                                                    12 => "December");
                                                                $month = $mons[$ddate[0]];
                                                                ?>
                                                                <a href="#"><i class="icon-present"></i><?php echo $month." ".$ddate[1].", ".$ddate[2]; ?></a>
                                                            <?php else: ?>
                                                                <a href="#"><i class="icon-present"></i>N / A</a>
                                                            <?php endif; ?>

                    									</li>
                    									<li>
                    										<a href="#">
                    										<i class="icon-home" style="display: inline-block;width: 20px;vertical-align: top;margin-top: 5px;"></i>
                                                            <div style="display: inline-block; width: 230px;"><?php echo $dcandidate["address"]; ?></div></a>
                    									</li>
                    								</ul>
                    							</div>
                    							<!-- END MENU -->
                    						</div>
                    						<!-- END PORTLET MAIN -->
                    					</div>
                    					<!-- END BEGIN PROFILE SIDEBAR -->
                    					<!-- BEGIN PROFILE CONTENT -->
                    					<div class="profile-content">
                                            <div class="timeline">
                                				<!-- TIMELINE ITEM -->
                                				<div class="timeline-item">
                                					<div class="timeline-badge">
                                						<div class="timeline-icon">
                                							<i class="icon-star font-green-haze"></i>
                                						</div>
                                					</div>
                                					<div class="timeline-body">
                                						<div class="timeline-body-arrow">
                                						</div>
                                						<div class="timeline-body-head">
                                							<div class="timeline-body-head-caption">
                                								<span class="timeline-body-alerttitle font-red-intense">About Me</span>
                                								<!-- <span class="timeline-body-time font-grey-cascade">at 11:00 PM</span> -->
                                							</div>
                                						</div>
                                						<div class="timeline-body-content">
                                							<span class="font-grey-cascade"><?php echo $dcandidate["about"]; ?></span>
                                						</div>
                                					</div>
                                				</div>
                                                <div class="timeline-item">
                                                    <div class="timeline-badge">
                                                        <div class="timeline-icon">
                                                            <i class="icon-trophy font-green-haze"></i>
                                                        </div>
                                                    </div>
                                                    <div class="timeline-body">
                                                        <div class="timeline-body-arrow">
                                                        </div>
                                                        <div class="timeline-body-head">
                                                            <div class="timeline-body-head-caption">
                                                                <span class="timeline-body-alerttitle font-red-intense">My Goals</span>
                                                                <!-- <span class="timeline-body-time font-grey-cascade">at 11:00 PM</span> -->
                                                            </div>
                                                        </div>
                                                        <div class="timeline-body-content">
                                                            <span class="font-grey-cascade"><?php echo $dcandidate["goal"]; ?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                			</div>
                    					</div>
                    					<!-- END PROFILE CONTENT -->
                    				</div>
                                <?php else: ?>
                                    <?php $dcandids = unserialize($position->getAllPositions())?>
                                    <?php foreach ($dcandids as $key => $value): ?>
                                        <div class="col-md-4">
                                            <?php //print_r($value); ?>
                                            <div class="portlet light ">
            									<div class="portlet-title">
            										<div class="caption caption-md">
            											<i class="icon-bar-chart theme-font hide"></i>
            											<span class="caption-subject font-blue-madison bold uppercase"><?php echo $value["posname"]; ?></span>
            										</div>
                                                    <div class="actions">
                                                        <a href="<?php echo HOST; ?>/?section=positions&posid=<?php echo $value["posid"]; ?>" class="btn btn-icon-only btn-circle green-meadow" title="View by Chart"><i class="fa fa-bars"></i></a>
                                                        <a href="<?php echo HOST; ?>/?section=stats&posid=<?php echo $value["posid"]; ?>" class="btn btn-icon-only btn-circle blue" title="View by Area"><i class="fa fa-map-marker"></i></a>
            										</div>
            									</div>
            									<div class="portlet-body">
                                                    <div class="row number-stats margin-bottom-30" style="margin-bottom: 0 !important;">
                                                        <div class="stat-right">
                                                            <div class="stat-number">
                                                                <div class="title">People</div>
                                                                <div class="number"><?php echo $monitor->checknumberofcandidates($value["posid"]); ?></div>
                                                            </div>
                                                            <div class="stat-chart">
                                                                <div id="stat_<?php echo $value["posid"]; ?>"></div>
                                                            </div>
                                                        </div>
                                                    </div>
            									</div>
            								</div>

                                        </div>

                                    <?php endforeach; ?>
                                <?php endif; ?>
							</div>
						</div>
						<!-- END ROW -->
					</div>
				</div>
				<!-- END PAGE CONTENT-->
			</div>
		</div>
		<!-- END CONTENT -->
		<!-- BEGIN QUICK SIDEBAR -->
		<!--Cooming Soon...-->
		<!-- END QUICK SIDEBAR -->
	</div>
	<!-- END CONTAINER -->
</div>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo HOST; ?>/assets/global/plugins/respond.min.js"></script>
<script src="<?php echo HOST; ?>/assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="<?php echo HOST; ?>/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo HOST; ?>/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?php echo HOST; ?>/assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?php echo HOST; ?>/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo HOST; ?>/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<?php echo HOST; ?>/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo HOST; ?>/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo HOST; ?>/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="<?php echo HOST; ?>/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<?php echo HOST; ?>/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo HOST; ?>/assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
<script src="<?php echo HOST; ?>/assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
<script src="<?php echo HOST; ?>/assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
<script src="<?php echo HOST; ?>/assets/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
<script src="<?php echo HOST; ?>/assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
<script src="<?php echo HOST; ?>/assets/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
<script src="<?php echo HOST; ?>/assets/global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
<script src="<?php echo HOST; ?>/assets/global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
<script src="<?php echo HOST; ?>/assets/global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
<script src="<?php echo HOST; ?>/assets/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>
<script src="<?php echo HOST; ?>/assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo HOST; ?>/assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="<?php echo HOST; ?>/assets/admin/layout2/scripts/layout.js" type="text/javascript"></script>
<script src="<?php echo HOST; ?>/assets/admin/layout2/scripts/demo.js" type="text/javascript"></script>
<script src="<?php echo HOST; ?>/assets/admin/pages/scripts/charts-amcharts.js"></script>
<script>
jQuery(document).ready(function() {
   // initiate layout and plugins
   Metronic.init(); // init metronic core components
    Layout.init(); // init current layout
    Demo.init(); // init demo features
    ChartsAmcharts.init();
});
</script>
<script type="text/javascript">
<?php if(@$_GET["section"] == "positions"): ?>
    var chart = AmCharts.makeChart("homechart", {
        "theme": "light",
        "type": "serial",
        "startDuration": 2,

        "fontFamily": 'Open Sans',

        "color":    '#888',

        "dataProvider": [
            <?php if (@$_GET["section"] == "positions" && isset($_GET["posid"])): ?>
                <?php $dmonitoring = $monitor->getTotalVotesPerCandidates($_GET["posid"]); ?>
                <?php foreach (unserialize($dmonitoring) as $key => $value): ?>
                    {
                        "country": "<?php echo $value["candidname"]; ?>",
                        "visits": <?php echo $value["votes"]; ?>,
                        "color": "<?php echo $value["candidcolor"]; ?>",
                        "image" : "<?php echo ($value["image"] != "" ? $value["image"] : HOST."/uploads/default.png"); ?>"
                    },
                <?php endforeach; ?>
            <?php else: ?>
                {
                    "country": "USA",
                    "visits": 200,
                    "color": "#FF0F00"
                }, {
                    "country": "China",
                    "visits": 350,
                    "color": "#FF6600"
                }, {
                    "country": "Brazil",
                    "visits": 395,
                    "color": "#754DEB"
                }, {
                    "country": "Italy",
                    "visits": 386,
                    "color": "#DDDDDD"
                }, {
                    "country": "Australia",
                    "visits": 384,
                    "color": "#999999"
                }, {
                    "country": "Taiwan",
                    "visits": 338,
                    "color": "#333333"
                }, {
                    "country": "Poland",
                    "visits": 328,
                    "color": "#000000"
                }
            <?php endif; ?>
        ],
        "valueAxes": [{
            "position": "left",
            "axisAlpha": 0,
            "gridAlpha": 0
        }],
        "graphs": [{
            "balloonText": '<div class="mainhousing"><div class="mainimage"><img style="width:100%;max-width:150px;" src="[[image]]"></div><div class="submain">[[category]]: <b>[[value]]</b></div></div>',
            "colorField": "color",
            "fillAlphas": 0.85,
            "lineAlpha": 0.1,
            "type": "column",
            "topRadius": 1,
            "valueField": "visits"
        }],
        "depth3D": 40,
        "angle": 10,
        "chartCursor": {
            "categoryBalloonEnabled": false,
            "cursorAlpha": 0,
            "zoomable": false
        },
        "categoryField": "country",
        "categoryAxis": {
            "gridPosition": "start",
            "axisAlpha": 0,
            "gridAlpha": 0

        },
        "exportConfig": {
            "menuTop": "20px",
            "menuRight": "20px",
            "menuItems": [{
                "icon": '/lib/3/images/export.png',
                "format": 'png'
            }]
        }
    }, 0);

    jQuery('.homechart_chart_input').off().on('input change', function() {
        var property = jQuery(this).data('property');
        var target = chart;
        chart.startDuration = 0;

        if (property == 'topRadius') {
            target = chart.graphs[0];
        }

        target[property] = this.value;
        chart.validateNow();
    });

    $('#homechart').closest('.portlet').find('.fullscreen').click(function() {
        chart.invalidateSize();
    });



    <?php elseif(@$_GET["section"] == "stats" && isset($_GET["posid"])): ?>
        <?php $dmonitoring = $monitor->getTotalVotesPerCandidates($_GET["posid"]); ?>
        <?php foreach (unserialize($dmonitoring) as $key => $value): ?>

            <?php $dmonitor = $monitor->getvotes($value["candid"]); ?>
            <?php $dcandids = unserialize($dmonitor); ?>
            <?php
                $offset = "";
                $values = "";
                $count = 0;
            ?>
            <?php if(!empty($dcandids)): ?>
                <?php foreach (unserialize($dcandids["casts"]) as $key => $subval): ?>
                    <?php $values .= ($subval != "" ? $subval : "0").","; ?>
                    <?php $offset .= $count.":'".$key."',"; ?>
                    <?php $count++; ?>
                <?php endforeach; ?>
            <?php else: ?>
                <?php
                    for ($i = 1; $i <= 146; $i++) {
                        $offset .= "0 : 0,";
                        $values .= "0,";
                    }
                ?>
            <?php endif; ?>

            $("#postat_<?php echo $value["candid"]; ?>").sparkline([<?php echo $values; ?>], {
                type: 'bar',
                width: '100',
                barWidth: 5,
                height: '45',
                barColor: '#3598dc',
                negBarColor: '#e02222',
                tooltipFormat: "{{offset:offset}} : {{value:val}}",
                tooltipValueLookups: {
                    'offset': {<?php echo $offset; ?>}
                },
            });

        <?php endforeach; ?>

    <?php else: ?>
        <?php $dcandids = unserialize($position->getAllPositions())?>
        <?php foreach ($dcandids as $key => $value): ?>
            <?php $dmonitoring = $monitor->getTotalVotesPerCandidates($value["posid"]); ?>
            <?php
                $offset = "";
                $values = "";
                $count = 0;
            ?>
            <?php foreach (unserialize($dmonitoring) as $key => $subval): ?>
                <?php $values .= ($subval["votes"] != "" ? $subval["votes"] : "0").","; ?>
                <?php $offset .= $count.":'".$subval["candidname"]."',"; ?>
                <?php $count++; ?>
            <?php endforeach; ?>

            $("#stat_<?php echo $value["posid"]; ?>").sparkline([<?php echo $values; ?>], {
                type: 'bar',
                width: '100',
                barWidth: 7,
                height: '45',
                barColor: '#3598dc',
                negBarColor: '#e02222',
                tooltipFormat: "{{offset:offset}} : {{value:val}}",
                tooltipValueLookups: {
                    'offset': {<?php echo $offset; ?>}
                }
            });
        <?php endforeach; ?>
    <?php endif; ?>
</script>
<!-- END PAGE LEVEL SCRIPTS -->
</body>
<!-- END BODY -->
</html>
