<div class="page-sidebar-wrapper">
	<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
	<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
	<div class="page-sidebar navbar-collapse collapse">
		<!-- BEGIN SIDEBAR MENU -->
		<!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
		<!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
		<!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
		<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
		<!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
		<!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
		<div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 371px;">
            <ul class="page-sidebar-menu  page-sidebar-menu-compact" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" data-height="371" data-initialized="1" style="overflow: hidden; width: auto; height: 371px;">
			<li class="start <?php echo ( @$_GET['page'] == '' || @$_GET['page'] == 'dashboard' ? 'active' : ''); ?>">
				<a href="<?php echo HOST; ?>/?page=dashboard">
				<i class="icon-home"></i>
				<span class="title">Dashboard</span>
				<?php echo ( @$_GET['page'] == '' || @$_GET['page'] == 'dashboard' ? '<span class="selected"></span>' : ''); ?>
				</a>
			</li>
			<li class="<?php echo ( @$_GET['page'] == 'candidate' ? 'active open' : ''); ?>">
				<a href="javascript:;"><i class="icon-user"></i><span class="title">Candidates</span><span class="arrow "></span><?php echo ( @$_GET['page'] == 'student_info' ? '<span class="selected"></span>' : ''); ?></a>

				<ul class="sub-menu">
					<li><a href="<?php echo HOST ?>/?page=candidate&type=all">All Candidates</a></li>
					<li><a href="<?php echo HOST ?>/?page=candidate&type=add">Add Candidates</a></li>
				</ul>
			</li>
			<li class="<?php echo ( @$_GET['page'] == 'position' ? 'active open' : ''); ?>">
				<a href="javascript:;"><i class="icon-trophy"></i><span class="title">Positions</span><span class="arrow "></span><?php echo ( @$_GET['page'] == 'student_info' ? '<span class="selected"></span>' : ''); ?></a>

				<ul class="sub-menu">
					<li><a href="<?php echo HOST ?>/?page=position&type=all">All Positions</a></li>
					<li><a href="<?php echo HOST ?>/?page=position&type=add">Add Positions</a></li>
				</ul>
			</li>
			<li class="<?php echo ( @$_GET['page'] == 'area' ? 'active open' : ''); ?>">
				<a href="javascript:;"><i class="icon-globe"></i><span class="title">Area</span><span class="arrow "></span><?php echo ( @$_GET['page'] == 'student_info' ? '<span class="selected"></span>' : ''); ?></a>

				<ul class="sub-menu">
					<li><a href="<?php echo HOST ?>/?page=area&type=all">All Area</a></li>
					<li><a href="<?php echo HOST ?>/?page=area&type=add">Add Area</a></li>
				</ul>
			</li>
			<li class="<?php echo ( @$_GET['page'] == 'monitor' ? 'active open' : ''); ?>">
				<a href="javascript:;"><i class="icon-bar-chart"></i><span class="title">Monitoring</span><span class="arrow "></span><?php echo ( @$_GET['page'] == 'student_info' ? '<span class="selected"></span>' : ''); ?></a>

				<ul class="sub-menu">
					<?php $allpositions = $position->getAllPositions(); ?>
					<!-- <li><a href="<?php echo HOST ?>/?page=monitor&type=all">All Castings</a></li> -->
					<?php foreach (unserialize($allpositions) as $key => $value): ?>
						<li><a href="<?php echo HOST; ?>/?page=monitor&type=cast&pos=<?php echo $value["posid"]; ?>"><?php echo $value["posname"]; ?></a></li>
					<?php endforeach; ?>

					<!-- <li><a href="<?php echo HOST ?>/?page=monitor&type=add">Add Area</a></li> -->
				</ul>
			</li>
			<!-- <li class="<?php echo ( @$_GET['page'] == 'area' ? 'active open' : ''); ?>">
				<a href="javascript:;"><i class="icon-users"></i><span class="title">Users</span><span class="arrow "></span><?php echo ( @$_GET['page'] == 'student_info' ? '<span class="selected"></span>' : ''); ?></a>

				<ul class="sub-menu">
					<li><a href="<?php echo HOST ?>/?page=user&type=all">All Users</a></li>
					<li><a href="<?php echo HOST ?>/?page=user&type=add">Add Users</a></li>
				</ul>
			</li> -->
			<!-- <li class="last ">
				<a href="<?php echo HOST; ?>/?page=logout&type=admin"><i class="icon-logout"></i><span class="title">Sign Out</span></a>
			</li> -->
		</ul><div class="slimScrollBar" style="width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 168.885px; background: rgb(187, 187, 187);"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; opacity: 0.2; z-index: 90; right: 1px; background: rgb(234, 234, 234);"></div></div>
		<!-- END SIDEBAR MENU -->
	</div>
</div>
