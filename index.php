<?php include 'config.php'; ?>
<?php include 'functions/functions.php'; ?>
<?php if(!isset($_GET['page'])): ?>
    <?php include 'template/home.php';?>
<?php elseif($_GET['page'] == "login-admin"): ?>
    <?php include 'template/login-admin.php';?>
<?php elseif($_GET['page'] == "logout"): ?>
    <?php include 'template/logout.php';?>
<?php else: ?>
    <?php include 'template/header.php'; ?>
    <?php include 'template/page.php';?>
    <?php include 'template/footer.php'; ?>
<?php endif; ?>
